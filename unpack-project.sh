#!/usr/bin/env bash

cp .env.example .env

mkdir containers

cd containers

git clone git@gitlab.com:logging-system/go-server.git

git clone git@gitlab.com:logging-system/frontend.git

git clone git@gitlab.com:logging-system/nginx.git

cd ../

docker-compose up -d

docker-compose down
