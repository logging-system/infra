#!/usr/bin/env bash

docker run -d --restart always --network log-system-network --name mongo -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=toor -v /home/gitlab-runner/data/mongo:/data/db mongo:latest
